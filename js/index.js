const media = condition => (window.matchMedia(condition).matches);

// config
gsap.registerPlugin(ScrollTrigger);

// navbar
const burger = document.querySelector('.header__burger');
const navbar = document.querySelector('.header__navbar');
const popup = document.querySelector('.header__popup');

burger.addEventListener('click', () => {
    burger.classList.toggle('active');
    navbar.classList.toggle('active');
    popup.classList.toggle('active');
});

popup.addEventListener('click', () => {
    burger.classList.remove('active');
    navbar.classList.remove('active');
    popup.classList.remove('active');
});

// section header
if (media("(min-width: 576px)")) {
    gsap.from('.header__item', { duration: 0.5, x: -1500, stagger: 0.3 });
}
if (media("(max-width: 576px)")) {
    gsap.from('.header__logo', { duration: 0.6, x: -1000 });
    gsap.from('.header__burger', { duration: 0.6, x: 1000 });
}

// section page
gsap.from('.hero__title', {
    duration: 1, x: -2000, scrollTrigger: {
        trigger: ".hero",
        toggleActions: "restart none none none",
    }, ease: "bounce.out", y: 600
});
gsap.from('.hero__description', {
    duration: 1.1, x: -2000, scrollTrigger: {
        trigger: ".hero",
        toggleActions: "restart none none none",
    }
});
gsap.from('.hero__link', {
    duration: 1, y: 2000, scrollTrigger: {
        trigger: ".hero",
        toggleActions: "restart none none none",
    }
});

// section about
gsap.from('.about .subtitle', {
    duration: 1, y: 600, opacity: 0, scrollTrigger: {
        trigger: ".about",
        toggleActions: "restart none none none",
    }
});
gsap.from('.about__text--one', {
    duration: 1, y: 600, opacity: 0, scrollTrigger: {
        trigger: ".about",
        toggleActions: "restart none none none",
    }
});
gsap.from('.about__text--two', {
    duration: 1, y: 600, opacity: 0, scrollTrigger: {
        trigger: ".about",
        toggleActions: "restart none none none",
    }
});

// skills
gsap.from('.skills .subtitle', {
    duration: 1, x: -500, opacity: 0, scrollTrigger: {
        trigger: ".skills",
        toggleActions: "restart none none none",
    }, ease: "expo.out", y: -500, 
});
gsap.from('.skills__photo', {
    duration: 1, x: -1000, opacity: 0, scrollTrigger: {
        trigger: ".skills",
        toggleActions: "restart none none none",
    }, ease: "expo.out", y: 500, 
});
gsap.from('.skills__sections', {
    duration: 0.9, x: 1000, opacity: 0, scrollTrigger: {
        trigger: ".skills__sections",
        toggleActions: "restart none none none",
    }, stagger: 0.3,
});

// portfolio
gsap.from('.portfolio .subtitle', {
    duration: 0.9, y: -1000, opacity: 0, scrollTrigger: {
        trigger: ".portfolio",
        toggleActions: "restart none none none",
    }, 
});
gsap.from('.portfolio__name', {
    duration: 0.9, x: -1000, opacity: 0, scrollTrigger: {
        trigger: ".portfolio",
        toggleActions: "restart none none none",
    }, 
});
gsap.from('.portfolio__description', {
    duration: 0.9, y: 1000, opacity: 0, scrollTrigger: {
        trigger: ".portfolio",
        toggleActions: "restart none none none",
    }, 
});
gsap.from('.portfolio__block--one img', {
    duration: 0.9, x: -1000, opacity: 0, scrollTrigger: {
        trigger: ".portfolio",
        toggleActions: "restart none none none",
    },  stagger: 0.3,
});
gsap.from('.portfolio__block--two img', {
    duration: 0.9, x: 1000, opacity: 0, scrollTrigger: {
        trigger: ".portfolio",
        toggleActions: "restart none none none",
    },  stagger: 0.3,
});

// footer 
gsap.from(".footer .subtitle", {
    duration: 1, x: 0, scrollTrigger: {
        trigger: ".footer",
        toggleActions: "restart none none none",
    }, ease: "elastic.out(1,0.3)", y: 600,
})
gsap.from(".footer__network", {
    duration: 1, x: 0, scrollTrigger: {
        trigger: ".footer",
        toggleActions: "restart none none none",
    }, ease: "bounce.out", y: 600,
})
gsap.from(".footer__info--email", {
    duration: 0.8, x: -1000, scrollTrigger: {
        trigger: ".footer__info--email",
        toggleActions: "restart none none none",
    }
})
gsap.from(".footer__info--phone", {
    duration: 0.8, x: 1000, scrollTrigger: {
        trigger: ".footer__info--phone",
        toggleActions: "restart none none none",
    }
})